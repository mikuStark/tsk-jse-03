# TASK MANAGER

SCREENSHPTS

https://disk.yandex.ru/d/tHChdvf_iRGuxw?w=1

## DEVELOPER INFO

name: Mariya Karbainova

e-mail: mariya@karbainova

## HAEDWARE

CPU: i5

RAM: 16G

SSD: 512GB

## SOFTWARE

Version JDK: 1.8.0_281

## PROGRAM RUN

```bash
java -jar ./task-manager.jar
```
